# Automation priority: null
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource
Library		squash_tf.TFParamService

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_561_SETUP}	Get Variable Value	${TEST 561 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_561_SETUP is not None	${__TEST_561_SETUP}

Test Teardown
	${__TEST_561_TEARDOWN}	Get Variable Value	${TEST 561 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_561_TEARDOWN is not None	${__TEST_561_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
alpha3
	${input} =	Get Test Param	DS_input
	${output} =	Get Test Param	DS_output

	[Setup]	Test Setup

	Given String "a" is set to ${input}
	And String "b" is set to ${output}
	When String "a" is reverted
	Then String "a" and "b" are equal

	[Teardown]	Test Teardown