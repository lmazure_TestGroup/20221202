import robot.api.logger
import robot.utils.asserts


allStrings = {}

def set_string_to(var, value):
    allStrings[var] = value

def revert_string(var):
    allStrings[var] = allStrings[var][::-1]

def uppercase_string(var):
    allStrings[var] = allStrings[var].upper()


def assert_strings_are_equal(var1, var2):
    robot.utils.asserts.assert_equal(allStrings[var1], allStrings[var2])
